<?php 

// [Section] Repetition Control Structures
	// It is used to execute code multiple times.

// While loop
	// A while loop takes a single condition.


function whileLoop(){
	$count = 5;

	while($count !== 0){
		echo $count."<br/>";
		$count--;
	}
}

// Do-While Loop
	// A do-while loop works a lot like the while loop. The only difference is that, do-while guarantee that the code will run/executed at least once.

function doWhileLoop(){
	$count = 20;

	do {
		echo $count."<br/>";
		$count--;
	} while ($count > 20);
}

// For Loop
	/*
		for(initialValue; condition; iteration){
			// code block
		}
	*/

function forLoop(){
	for($count = 0; $count <= 20; $count++){
		echo "$count". '<br/>';
	}
}

// Countinue and Break Statements
	// "Continue" is a keyword that allows the code to go to the next loop without finish the current code block
	// "Break" on the other hand is a keyword that stop the execution of the current loop.

function modifiedForLoop(){
	for($count = 0; $count <= 20; $count++){
		// if the count is divisible by 2, do this;
		if($count % 2 === 0){
			continue;
		}
		// if not just continue the iteration
		echo "$count"."<br/>";

		// if the count is greater than 10 stops iteration
		if($count > 10){
			break;
		}
	}
}

// [Section] Array Manipulation
	// An array is a kind of variable that can hold more than one value.
	// Arrays are declared using array() function or square brackets '[]'.
	// In the earlier version of php, we cannot used [], but as of php 5.4 we can used the short array syntax which replace array() with [].

// Before the php 5.4
$studentNumbers = array("2020-1923", "2020-1924", "2020-1965", "2020-1926", "2020-1927");
// After the php 5.4
$studentNumbers = ["2020-1923", "2020-1924", "2020-1965", "2020-1926", "2020-1927"];

// Simple Arrays
$grades = [98.5, 94.3, 89.2, 90.1];
$computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"];
$tasks = [
	"drink html",
	"eat php",
	"inhale css",
	"bake javascript"
];

// Associative Array
	// Associative Array differ from numeric array in the sense that associative arrays uses decriptive names in naming the elements/values (key=>value pair).

$gradePeriods = ["firstGrading" => 98.5, "secondGrading" => 94.3, "thirdGrading" => 89.2, "fourthGrading" => 90.1];

// Two-Dimensional Array
	// Two dimensional array is commonly used in image processing, good example of this is our viewing screen the uses multidimensional array of pixels.
$heroes = [
	["iron man", "thor", "hulk"],
	["wolverine", "cyclops", "jean grey"],
	["batman", "superman", "wonder woman"]
];

// Two-Dimensional Associative Array

$ironManPowers = [
	"regular" => ["repulsor blast", "rocket punch"],
	"signature" => ["unibeam"]
];

// Array Iterative method: foreach();
// foreach($heroes as $hero){
// 	print_r($hero);
// }

// [Section] Array Mutators
	// Array Mutators modify the contents of an array.

// Array Sorting

$sortedBrands = $computerBrands;
$reverseSortedBrands = $computerBrands;

sort($sortedBrands);
rsort($reverseSortedBrands);

?>