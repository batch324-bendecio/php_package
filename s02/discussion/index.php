<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S02: Selection Control Structure and Array Manipulation</title>
	</head>

	
	<body>

		<h1>Repetition Control Structure</h1>
		<h3>While loop</h3>
		<?php whileLoop(); ?>

		<h3>Do-while Loop</h3>
		<?php doWhileLoop(); ?>

		<h3>For Loop</h3>
		<?php forLoop(); ?>

		<h3>Continue and Break</h3>
		<?php modifiedForLoop(); ?>

		<h1>Array Manipulation</h3>

		<h3>Simple Array</h3>

		<ul>
			<!-- php codes/statements can be breakdown using php tags -->
			<?php foreach($computerBrands as $brand){
			?>	
				<li><?= $brand; ?></li>
			<?php 
			} ?>
		</ul>

		<h3>Associative Arrays</h3>

		<ul>
			<?php foreach($gradePeriods as $period => $grade){ ?>
				<li>Grade in <?= $period ?> is <?= $grade ?> </li>
			<?php } ?>
		</ul>

		<h3>Multidimensional Array</h3>

		<ul>
			<?php 
				// Each $heroes will be represented by a $team (accessing the outer array elements)
				foreach($heroes as $team){
					// Each $team will be represented by a $member (accessing the inner array elements)
					foreach($team as $member){
						?> 
							<li><?php echo $member ?></li>
						<?php
					}
				}
			?>
		</ul>

		<h3>Multi-Dimensional Associative Array</h3>
			<ul>
				<?php 
					foreach($ironManPowers as $label => $powerGroup){
						foreach($powerGroup as $power){
							?> 
								<li><?= "$label: $power" ?></li>
							<?php
						}
					}
				?>
			</ul>

		<h1>Mutators</h1>
		<h3>Sorting</h3>
		<p><?php print_r($sortedBrands); ?></p>
		<p><?php print_r($reverseSortedBrands); ?></p>

		<h3>Push</h3>
		<?php array_push($computerBrands, "Apple"); ?>
		<p><?php print_r($computerBrands); ?></p>

		<h3>Unshift</h3>
		<?php array_unshift($computerBrands, "Dell"); ?>
		<p><?php print_r($computerBrands); ?></p>

		<h3>Pop</h3>
		<?php array_pop($computerBrands); ?>
		<p><?php print_r($computerBrands); ?></p>

		<h3>Shift</h3>
		<?php array_shift($computerBrands); ?>
		<p><?php print_r($computerBrands); ?></p>

		<h3>Count</h3>
		<p><?php echo count($computerBrands); ?></p>

		<h3>In Array</h3>
		<p><?php var_dump(in_array("Acer", $computerBrands)) ?></p>
	</body>
</html>