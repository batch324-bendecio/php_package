<?php require_once("./code.php"); ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S02 Activity - Selection Control Structure and Array Manipulation</title>
	</head>


	<body>
		<h1>Divisible of 5</h1>
		<p><?php divisibleByFive(); ?></p>

		<h1>Array Manipulation</h1>
		<?php array_push($students, "John Smith"); ?>
		<p><?php var_dump($students); ?></p>
		<p><?php echo count($students); ?></p>
		<?php array_unshift($students, "Jane Smith"); ?>
		<p><?php var_dump($students); ?></p>
		<p><?php echo count($students); ?></p>
		<?php array_pop($students); ?>
		<p><?php var_dump($students); ?></p>
		<p><?php echo count($students); ?></p>


	</body>
</html>