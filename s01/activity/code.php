<?php 

function getFullAddress($country, $city, $province, $specificAddress){

	return "$specificAddress, $city, $province, $country";
}

function getLetterGrade($grades){
	if($grades >= 0 && $grades < 75){
		return $grades. " is equivalent to F";
	} else if($grades >= 75 && $grades <= 76){
		return $grades. " is equivalent to C-";
	} else if($grades >= 77 && $grades <= 79){
		return $grades. " is equivalent to C";
	} else if($grades >= 80 && $grades <= 82){
		return $grades. " is equivalent to C+";
	} else if($grades >= 83 && $grades <= 85){
		return $grades. " is equivalent to B-";
	} else if($grades >= 86 && $grades <= 88){
		return $grades. " is equivalent to B";
	} else if($grades >= 89 && $grades <= 91){
		return $grades. " is equivalent to B+";
	} else if($grades >= 92 && $grades <= 94){
		return $grades. " is equivalent to A-";
	} else if($grades >= 95 && $grades <= 97){
		return $grades. " is equivalent to A";
	} else if($grades >= 98 && $grades <= 100){
		return $grades. " is equivalent to A+";
	} else {
		return "Use only 0 to 100";
	}
}



 ?>