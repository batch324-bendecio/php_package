<?php require_once("./code.php"); ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>PHP Activity</title>
	</head>


	<body>

		<h1>Activity 1</h1>
		<h2>Full Address</h2>
		<p><?php echo getFullAddress("Philippines", "Quezon City", "Metro Manila", "3F Caswyn Bldg., Timog Avenue"); ?></p>
		<p><?php echo getFullAddress("Philippines", "Makati City", "Metro Manila", "3F Enzo Bldg., Buendia Avenue"); ?></p>

		<h1>Activity 2</h1>
		<h2>Letter-Based Grading</h2>
		<p><?php echo getLetterGrade(87) ?></p>
		<p><?php echo getLetterGrade(94) ?></p>
		<p><?php echo getLetterGrade(74) ?></p>


	</body>
</html>